from django import template
import math
import cmath

register = template.Library()


@register.filter
def value_(val):
    return round(math.cos(cmath.polar(complex(val))[0]), 4)
