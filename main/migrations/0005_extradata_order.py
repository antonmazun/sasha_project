# Generated by Django 3.1.4 on 2020-12-23 16:46

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0004_exportfile_extradata'),
    ]

    operations = [
        migrations.AddField(
            model_name='extradata',
            name='order',
            field=models.IntegerField(default=1),
        ),
    ]
