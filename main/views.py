import cmath

from django.shortcuts import render, redirect
from pyexcel_ods import get_data
from .models import File, Data, ExtraData
import json


# Create your views here.

def group_data(array):
    grouped_data = {}
    areas_in_dict = 0
    for data_list in array:
        if (data_list[0] == "year"):
            continue
        if areas_in_dict < 27 and not (data_list[1] in grouped_data.keys()):
            grouped_data.update({data_list[1]: {2017: {}, 2018: {}}})
            areas_in_dict += 1
        if not (data_list[5] in grouped_data.get(data_list[1]).get(data_list[0])):
            grouped_data.get(data_list[1]).get(data_list[0]).update({data_list[5]: {"list": [], "max": 0}})
        grouped_data.get(data_list[1]).get(data_list[0]).get(data_list[5]).get("list").append(data_list[3])
        if (grouped_data.get(data_list[1]).get(data_list[0]).get(data_list[5]).get("max") < data_list[3]):
            grouped_data.get(data_list[1]).get(data_list[0]).get(data_list[5]).update({"max": data_list[4]})

    return grouped_data


REGIONS = ['Київська', 'Луганська', 'Львівська', 'Миколаївська', 'Одеська', 'Полтавська', 'Рівненська', 'Сумська',
           'Тернопільська', 'Вінницька', 'Волинська', 'Закарпатська', 'Запорізька', 'Житомирська', 'Чернігівська',
           'Черкаська', 'Чернівецька', 'Дніпропетровська', 'Донецька', 'Івано-Франківська', 'Київ', 'Кіровоградська',
           'Харківська', 'Хмельницька', 'Херсонська', ]

import statistics


def nvl(value, koeff):
    try:
        return float(value)
    except:
        return koeff


import math


def nvl_morm(values):
    z = values[0]
    av = statistics.mean(values)
    sv = statistics.stdev(values)
    zz = 1 / (1 + math.exp((av - z) / sv))
    if zz:
        return 0.5
    else:
        return zz


def main(request):
    ctx = {
        'regions': REGIONS,
        'result': {}
    }
    result = {}
    if request.method == 'POST':
        file = request.FILES['file']
        region = request.POST['region']

        File.objects.create(file=file, select_region=region)
        data = get_data(file)
        clean_data = group_data(data['Sheet4'])
        target_region = clean_data[region]
        result = {}
        with open('file.txt', 'w') as f:
            print(target_region, file=f)
        data_2017 = target_region[2017]
        data_2018 = target_region[2018]
        VRP_LIST = [data_2017['VRP']['list'][0], data_2018['VRP']['list'][0]]
        max_val_vrp = max(VRP_LIST)

        ORPP_LIST = [data_2017['ORPP']['list'][0], data_2018['ORPP']['list'][0]]
        max_val_orpp = max(ORPP_LIST)

        PSG_LIST = [data_2017['PSG']['list'][0], data_2018['PSG']['list'][0]]
        max_val_psg = max(PSG_LIST)

        ORP_LIST = [data_2017['ORP']['list'][0], data_2018['ORP']['list'][0]]
        max_val_orp = max(ORP_LIST)

        KAP_LIST = [data_2017['KAP']['list'][0], data_2018['KAP']['list'][0]]
        max_val_kap = max(KAP_LIST)

        OORT_LIST = [data_2017['OORT']['list'][0], data_2018['OORT']['list'][0]]
        max_val_oort = max(OORT_LIST)

        SMZP_LIST = [data_2017['SMZP']['list'][0], data_2018['SMZP']['list'][0]]
        max_val_smzp = max(SMZP_LIST)

        EAN_LIST = [data_2017['EAN']['list'][0], data_2018['EAN']['list'][0]]
        max_val_ean = max(EAN_LIST)
        min_val_ean = min(EAN_LIST)

        PPP_LIST = [data_2017['PPP']['list'][0], data_2018['PPP']['list'][0]]
        max_val_ppp = max(PPP_LIST)

        ONR_LIST = [data_2017['ONR']['list'][0], data_2018['ONR']['list'][0]]
        max_val_onr = max(ONR_LIST)

        OVID_LIST = [data_2017['OVID']['list'][0], data_2018['OVID']['list'][0]]
        max_val_ovid = max(OVID_LIST)

        ORIP_LIST = [data_2017['ORIP']['list'][0], data_2018['ORIP']['list'][0]]
        max_val_orip = max(ORIP_LIST)

        KI_LIST = [data_2017['KI']['list'][0], data_2018['KI']['list'][0]]
        max_val_ki = max(KI_LIST)

        CaPOTL = [data_2017.get('CaPOTL', {'list': [None]})['list'][0],
                  data_2018.get('CaPOTL', {'list': [None]})['list'][0]]
        max_val_ki = max(KI_LIST)

        for year, data in target_region.items():
            VRP = data['VRP']['list'][0]
            VNP = 0.333 * nvl(VRP / max_val_vrp, 0.5)

            ORPP = data['ORPP']['list'][0]

            PSG = data['PSG']['list'][0]
            VSF = 0.056 * nvl(ORPP / max_val_orpp, 0.5) + 0.278 * nvl(PSG / max_val_psg, 0.5)

            ORP = data['ORP']['list'][0]

            SP = 0.333 * nvl(ORP / max_val_orp, 0.5)

            Ibp = VNP + VSF + SP

            KAP = data['KAP']['list'][0]

            PA = 0.334 * nvl(KAP / max_val_kap, 0.5)
            OORT = data['OORT']['list'][0]
            SR = 0.333 * nvl(OORT / max_val_oort, 0.5)

            OET = data['OET']['list'][0]
            OEP = data['OEP']['list'][0]

            OETP = OET + OEP
            max_val_oetp = max([OETP])
            OIT = data['OIT']['list'][0]
            OIP = data['OIP']['list'][0]
            OITP = OIT + OIP
            maxval_oitp = max([OITP])

            ZED = 0.205 * nvl(OETP / max_val_oetp, 0.5) + 0.128 * nvl(OITP / maxval_oitp, 0.5)

            Ipd = PA + SR + ZED
            SMZP = data['SMZP']['list'][0]
            ZP = 0.167 * nvl(SMZP / max_val_smzp, 0.5)

            EAN = data['EAN']['list'][0]

            EANA = 0.333 * nvl((1 - (max_val_ean - EAN) / (max_val_ean - min_val_ean)), 0.5)

            PPP = data['PPP']['list'][0]

            PRP = 0.5 * nvl(PPP / max_val_ppp, 0.5)

            Irp = ZP + EANA + PRP

            ONR = data['ONR']['list'][0]
            OVID = data['OVID']['list'][0]

            ID = 0.145 * nvl(ONR / max_val_onr, 0.5) + 0.105 * nvl(OVID / max_val_ovid, 0.5)

            ORIP = data['ORIP']['list'][0]

            RIP = 0.25 * nvl(ORIP / max_val_orip, 0.5)

            KI = data['KI']['list'][0]

            KIN = 0.5 * nvl(KI / max_val_ki, 0.5)
            Iiim = ID + RIP + KIN

            Iksp = 0.6 * Ibp + 0.4 * Ipd

            Iikp = 0.3 * Irp + 0.7 * Iiim

            Iec = (Iksp + Iikp) / 2
            # AIR17I = NVL_norm(from_ua("AIR201"));
            AIR17I = data['AIR201']['list'][0]
            AIR17I = 1 - AIR17I

            LRS17I = data['LRS201']['list'][0]

            BRS17I = data['BRS171']['list'][0]
            FFA201_norm = data['FFA201']['list'][0]
            FFA202_norm = data['FFA202']['list'][0]
            FFA203_norm = data['FFA203']['list'][0]
            HZD171_norm = data['HZD171']['list'][0]

            NTS20I = 0.4 * (FFA201_norm) + \
                     0.2 * (FFA202_norm) + \
                     0.1 * (FFA203_norm) + \
                     0.3 * (HZD171_norm)

            WAC171 = data['WAC171']['list'][0]
            WAC192 = data['WAC192']['list'][0]

            WAC17I = ((WAC171) + (WAC192)) / 2

            EMP171 = data['EMP171']['list'][0]
            EMP202 = data['EMP202']['list'][0]

            EMP20I = EMP171 + EMP202
            WTP171 = data['WTP171']['list'][0]
            WTP172 = data['WTP172']['list'][0]

            WTP17I = ((WTP171) + (WTP172)) / 2

            LNP171 = data['LNP171']['list'][0]
            LNP192 = data['LNP192']['list'][0]

            LNP17I = ((LNP171) + (LNP192)) / 2

            CCM17I = data['CCM201']['list'][0]
            CCM17I = 1 - CCM17I

            RWE201 = data['RWE201']['list'][0]
            RWE202 = data['RWE202']['list'][0]
            # змінювання клімату
            RWE17I = ((RWE201) + (RWE202)) / 2

            WSU171 = data['WSU171']['list'][0]
            WSU172 = data['WSU172']['list'][0]

            # вода
            WSU17I = ((WSU171) + (WSU172)) / 2

            EST17C = (AIR17I + LRS17I + BRS17I + NTS20I + WAC17I) / 5

            EPR17C = ((EMP20I) + (WTP17I) + (LNP17I)) / 3

            EMN17C = (CCM17I + RWE17I + WSU17I) / 3

            Ie = (EST17C + EPR17C + EMN17C) / 3

            AIDS_norm = data['AIDS']['list'][0]  # <-array(unlist(subset(ua, index_abbr == "AIDS", select = val)) )
            TSR_norm = data['TSR']['list'][0]

            HLT = (AIDS_norm + TSR_norm) / 2

            EL_norm = data['EL']['list'][0]
            LM_norm = data['LM']['list'][0]

            Ihd = 3 / 8 * (HLT) + 3 / 8 * (EL_norm) + 0.25 * (LM_norm)

            try:
                PATLP = data['PATLP']['list'][0]
            except:
                PATLP = 0.5

            try:
                PATLSC = data['PATLSC']['list'][0]
            except:
                PATLSC = 0.5

            try:
                PATLG = data['PATLG']['list'][0]
            except:
                PATLG = 0.5

            try:
                PATLLA = data['PATLLA']['list'][0]
            except:
                PATLLA = 0.5

            try:
                WPSPA = data['WPSPA']['list'][0]
            except:
                WPSPA = 0.5
            try:
                CFL = data['CFL']['list'][0]
            except:
                CFL = 0.5
            try:
                PR = data['PR']['list'][0]
            except:
                PR = 0.5

            PCP = (PATLP +
                   PATLSC +
                   PATLG +
                   PATLLA +
                   WPSPA +
                   CFL +
                   PR) / 7
            MPO_norm = data['MPO']['list'][0]
            SA_norm = data['SA']['list'][0]
            print(year, '!!!!!!!!!!!!')
            try:

                CaPOTL_norm = data['CaPOTL']['list'][0]
            except:
                CaPOTL_norm = 0.5

            DCS = (MPO_norm + SA_norm + CaPOTL_norm) / 3
            MmTL = data['MmTL']['list'][0]
            IU = data['IU']['list'][0]
            CS = data['CS']['list'][0]
            MmINT = data['MmINT']['list'][0]
            DIS = (MmTL + IU + MmINT) / 4
            NRO_norm = data['NRO']['list'][0]
            CTL_norm = data['CTL']['list'][0]
            REL = ((NRO_norm) + (CTL_norm)) / 2
            IID = 0.35 * (PCP) + 0.25 * (DCS) + 0.3 * (DIS) + 0.1 * (REL)
            try:

                PPBD = data['PPBD']['list'][0]
            except:
                PPBD = 0.5
            try:
                SHF = data['SHF']['list'][0]
            except:
                SHF = 0.5
            try:

                AHP = data['AHP']['list'][0]
            except:
                AHP = .5
            try:

                NCPU = data['NCPU']['list'][0]
            except:
                NCPU = 0.5
            try:

                PSPTO = data['PSPTO']['list'][0]
            except:
                PSPTO = 0.5
            try:

                CPED = data['CPED']['list'][0]
            except:
                CPED = 0.5
            INF = ((PPBD) +
                   (SHF) +
                   (AHP) +
                   (NCPU) +
                   (PSPTO) +
                   (CPED)) / 6
            DOGCO = data['DOGCO']['list'][0]
            COV = data['COV']['list'][0]
            NMBE = data['NMBE']['list'][0]
            RS = ((DOGCO) + (COV) + (NMBE)) / 3
            try:
                LS = data['LS']['list'][0]
            except:
                LS = 0.5
            try:
                Illw = data['Illw']['list'][0]
            except:
                Illw = 0.5
            try:
                FNADAM = data['FNADAM']['list'][0]
            except:
                FNADAM = 0.5

            try:
                GRP = data['GRP']['list'][0]
            except:
                GRP = 0.5
            EW = ((LS) + (Illw) + (FNADAM) + (GRP)) / 4
            Iql = 0.35 * INF + 0.35 * RS + 0.3 * EW
            Is = (Ihd + IID + Iql) / 3
            Cql = math.sqrt(Ie ** 2 + Is ** 2 + Iec ** 2)

            try:
                BN = data['BN']['list'][0]
            except:
                BN = 0.5

            try:
                KPVM = data['KPVM']['list'][0]
            except:
                KPVM = 0.5

            try:
                DNO = data['DNO']['list'][0]
            except:
                DNO = 0.5

            Sec = ((BN) ** 3 + (KPVM) ** 3 + (DNO) ** 3) ** (1 / 3)

            try:
                WRS19 = data['WRS19']['list'][0]
            except:
                WRS19 = 0.5

            try:
                POL19 = data['POL19']['list'][0]
            except:
                POL19 = 0.5
            try:
                CCH19 = data['CCH19']['list'][0]
            except:
                CCH19 = 0.5
            Se = ((WRS19) + (POL19) + (CCH19)) / 3

            try:
                mortTB = 1 - data['mortTB']['list'][0]
            except:
                mortTB = 0.5

            try:
                mortCANC = 1 - data['mortCANC']['list'][0]
            except:
                mortCANC = 0.5

            try:
                mortCIRCUL = 1 - data['mortCIRCUL']['list'][0]
            except:
                mortCIRCUL = 0.5

            try:
                mortAIDS = 1 - data['mortAIDS']['list'][0]
            except:
                mortAIDS = 0.5

            HEALTH_DEC = 0.2359512471 * (mortTB) + 0.236311309 * (mortCANC) + 0.2899148047 * (
                mortCIRCUL) + 0.2378226392 * (mortAIDS)

            try:
                CORR_PER = data['CFL']['list'][0]
            except:
                CORR_PER = 0.5

            try:
                LIFE_EXP = data['LIFE_EXP']['list'][0]
            except:
                LIFE_EXP = 0.5

            try:
                CRIME = data['CRIME']['list'][0]
            except:
                CRIME = 0.5

            try:
                CIR = data['CIR']['list'][0]
            except:
                CIR = 0.5
            # корупція
            Ss = ((LIFE_EXP) ** 3 +
                  (CRIME) ** 3 +
                  (CORR_PER) ** 3 +
                  (CIR) ** 3 +
                  (HEALTH_DEC) ** 3) ** (1 / 3)

            TH_N_CURR_Y = 'TH_N_{}'.format(year)

            TH_N_CURR = data[TH_N_CURR_Y]['list'][0]

            TH_N_PREV1_Y = 'TH_N_{}'.format(year - 1)
            TH_N_PREV1 = data[TH_N_PREV1_Y]['list'][0]
            TH_N_PREV2_Y = 'TN_H_{}'.format(year - 2)
            try:

                TH_N_PREV2 = data[TH_N_PREV2_Y]['list'][0]
            except:
                TH_N_PREV2 = 0.5
            TH = ((TH_N_PREV2) + (TH_N_PREV1) + (TH_N_CURR)) / 3  # TH = (TH_N_PREV2_Y + TH_N_PREV1_Y + TH_N_CURR_Y) / 3
            if TH:
                So = 0.5
            else:
                So = TH

            Csl = (Ss ** 3 + Sec ** 3 + Se ** 3 + So ** 3) ** (1 / 3)

            print('Csl', Csl)
            print('Ie', Ie)
            print('Is', Is)
            print('Iec', Iec)
            from cmath import sqrt
            Isd = sqrt((1 / 12 ** (1 / 3) * Csl) ** 2 + Ie ** 2 + Is ** 2 + Iec ** 2)

            G = 1 - math.acos((Iec + Ie + Is) / (math.sqrt(3.0) * math.sqrt(Iec ** 2 + Ie ** 2 + Is ** 2))) / math.acos(
                1 / math.sqrt(3))

            Q = sqrt(1 ** 2 * Csl ** 2 + (Ie ** 2 + Iec ** 2 + Is ** 2))
            result[year] = dict(year=year, region_name=region, Isd=round(math.cos(cmath.polar(complex(Isd))[0]), 4),
                                G=round(G, 4))
            last_record = ExtraData.objects.all().last()
            if last_record:

                ExtraData.objects.create(
                    LNP17I=LNP17I,
                    Ihd=Ihd,
                    WTP17I=WTP17I,
                    So=So,
                    Iql=Iql,
                    RWE17I=RWE17I,
                    Ss=Ss,
                    region=region,
                    g=G,
                    isd=Isd,
                    year=year,
                    order=last_record.order + 1
                )
            else:
                ExtraData.objects.create(
                    LNP17I=LNP17I,
                    Ihd=Ihd,
                    WTP17I=WTP17I,
                    So=So,
                    Iql=Iql,
                    RWE17I=RWE17I,
                    Ss=Ss,
                    region=region,
                    g=G,
                    isd=Isd,
                    year=year,
                    order=1
                )

            # Data.objects.create(year=year, select_region=region, isd=Isd, g=G)

        # VRP  =
        # maxval_vrp = max(VRP)
        # VNP  = 0.333 *
        # print(target_region)
        # for data in clean_data:
        #     print(data)
    ctx.update(result=result)
    return render(request, 'main.html', ctx)


def history_download(request):
    files = File.objects.all().order_by('-id')
    return render(request, 'history_download.html', {
        'files': files
    })


def data_by_region(request):
    region = request.GET.get('region')
    year = request.GET.get('year')
    if region and year:
        filter_fields = {
            'select_region': region,
            'year': year,
        }
    elif region and not year:
        filter_fields = {
            'select_region': region
        }
    elif year and not region:
        filter_fields = {
            'year': year
        }
    else:
        filter_fields = {}
    datas = Data.objects.filter(**filter_fields)
    list_data = list(datas)
    map_result  = {}
    for d in list_data:
        number  = math.cos(cmath.polar(complex(d.isd))[0])
        map_result.update({
            d.isd: number
        })
        print(d.isd , '!!!!')

    print('map_result ' , map_result)
    return render(request, 'data_by_region.html', {
        'datas': datas,
        'region': REGIONS,
        'map_result': map_result,
        'search': {
            'region': region,
            'year': year,
        }
    })


def export_data(request):
    result = {}
    if request.method == 'POST':
        file = request.FILES['file']
        File.objects.create(file=file, select_region='Всі')
        data = get_data(file)
        clean_data = group_data(data['Sheet4'])
        for region in REGIONS:
            target_region = clean_data[region]
            data_2017 = target_region[2017]
            data_2018 = target_region[2018]
            try:

                VRP_LIST = [data_2017['VRP']['list'][0], data_2018['VRP']['list'][0]]
            except:
                continue
            max_val_vrp = max(VRP_LIST)

            ORPP_LIST = [data_2017['ORPP']['list'][0], data_2018['ORPP']['list'][0]]
            max_val_orpp = max(ORPP_LIST)

            PSG_LIST = [data_2017['PSG']['list'][0], data_2018['PSG']['list'][0]]
            max_val_psg = max(PSG_LIST)

            ORP_LIST = [data_2017['ORP']['list'][0], data_2018['ORP']['list'][0]]
            max_val_orp = max(ORP_LIST)

            KAP_LIST = [data_2017['KAP']['list'][0], data_2018['KAP']['list'][0]]
            max_val_kap = max(KAP_LIST)

            OORT_LIST = [data_2017['OORT']['list'][0], data_2018['OORT']['list'][0]]
            max_val_oort = max(OORT_LIST)

            SMZP_LIST = [data_2017['SMZP']['list'][0], data_2018['SMZP']['list'][0]]
            max_val_smzp = max(SMZP_LIST)

            EAN_LIST = [data_2017['EAN']['list'][0], data_2018['EAN']['list'][0]]
            max_val_ean = max(EAN_LIST)
            min_val_ean = min(EAN_LIST)

            PPP_LIST = [data_2017['PPP']['list'][0], data_2018['PPP']['list'][0]]
            max_val_ppp = max(PPP_LIST)

            ONR_LIST = [data_2017['ONR']['list'][0], data_2018['ONR']['list'][0]]
            max_val_onr = max(ONR_LIST)

            OVID_LIST = [data_2017['OVID']['list'][0], data_2018['OVID']['list'][0]]
            max_val_ovid = max(OVID_LIST)

            ORIP_LIST = [data_2017['ORIP']['list'][0], data_2018['ORIP']['list'][0]]
            max_val_orip = max(ORIP_LIST)

            KI_LIST = [data_2017['KI']['list'][0], data_2018['KI']['list'][0]]
            max_val_ki = max(KI_LIST)

            CaPOTL = [data_2017.get('CaPOTL', {'list': [None]})['list'][0],
                      data_2018.get('CaPOTL', {'list': [None]})['list'][0]]
            max_val_ki = max(KI_LIST)
            result[region] = {"2017": {}, "2018": {}}
            for year, data in target_region.items():
                VRP = data['VRP']['list'][0]
                VNP = 0.333 * nvl(VRP / max_val_vrp, 0.5)

                ORPP = data['ORPP']['list'][0]

                PSG = data['PSG']['list'][0]
                VSF = 0.056 * nvl(ORPP / max_val_orpp, 0.5) + 0.278 * nvl(PSG / max_val_psg, 0.5)

                ORP = data['ORP']['list'][0]

                SP = 0.333 * nvl(ORP / max_val_orp, 0.5)

                Ibp = VNP + VSF + SP

                KAP = data['KAP']['list'][0]

                PA = 0.334 * nvl(KAP / max_val_kap, 0.5)
                OORT = data['OORT']['list'][0]
                SR = 0.333 * nvl(OORT / max_val_oort, 0.5)

                OET = data['OET']['list'][0]
                OEP = data['OEP']['list'][0]

                OETP = OET + OEP
                max_val_oetp = max([OETP])
                OIT = data['OIT']['list'][0]
                OIP = data['OIP']['list'][0]
                OITP = OIT + OIP
                maxval_oitp = max([OITP])

                ZED = 0.205 * nvl(OETP / max_val_oetp, 0.5) + 0.128 * nvl(OITP / maxval_oitp, 0.5)

                Ipd = PA + SR + ZED
                SMZP = data['SMZP']['list'][0]
                ZP = 0.167 * nvl(SMZP / max_val_smzp, 0.5)

                EAN = data['EAN']['list'][0]

                EANA = 0.333 * nvl((1 - (max_val_ean - EAN) / (max_val_ean - min_val_ean)), 0.5)

                PPP = data['PPP']['list'][0]

                PRP = 0.5 * nvl(PPP / max_val_ppp, 0.5)

                Irp = ZP + EANA + PRP

                ONR = data['ONR']['list'][0]
                OVID = data['OVID']['list'][0]

                ID = 0.145 * nvl(ONR / max_val_onr, 0.5) + 0.105 * nvl(OVID / max_val_ovid, 0.5)

                ORIP = data['ORIP']['list'][0]

                RIP = 0.25 * nvl(ORIP / max_val_orip, 0.5)

                KI = data['KI']['list'][0]

                KIN = 0.5 * nvl(KI / max_val_ki, 0.5)
                Iiim = ID + RIP + KIN

                Iksp = 0.6 * Ibp + 0.4 * Ipd

                Iikp = 0.3 * Irp + 0.7 * Iiim

                Iec = (Iksp + Iikp) / 2
                # AIR17I = NVL_norm(from_ua("AIR201"));
                AIR17I = data['AIR201']['list'][0]
                AIR17I = 1 - AIR17I

                LRS17I = data['LRS201']['list'][0]

                BRS17I = data['BRS171']['list'][0]
                FFA201_norm = data['FFA201']['list'][0]
                FFA202_norm = data['FFA202']['list'][0]
                FFA203_norm = data['FFA203']['list'][0]
                HZD171_norm = data['HZD171']['list'][0]

                NTS20I = 0.4 * (FFA201_norm) + \
                         0.2 * (FFA202_norm) + \
                         0.1 * (FFA203_norm) + \
                         0.3 * (HZD171_norm)

                WAC171 = data['WAC171']['list'][0]
                WAC192 = data['WAC192']['list'][0]

                WAC17I = ((WAC171) + (WAC192)) / 2

                EMP171 = data['EMP171']['list'][0]
                EMP202 = data['EMP202']['list'][0]

                EMP20I = EMP171 + EMP202
                WTP171 = data['WTP171']['list'][0]
                WTP172 = data['WTP172']['list'][0]

                # забруднення повітря
                WTP17I = ((WTP171) + (WTP172)) / 2

                LNP171 = data['LNP171']['list'][0]
                LNP192 = data['LNP192']['list'][0]

                # водне навантаження
                LNP17I = ((LNP171) + (LNP192)) / 2

                CCM17I = data['CCM201']['list'][0]
                CCM17I = 1 - CCM17I

                RWE201 = data['RWE201']['list'][0]
                RWE202 = data['RWE202']['list'][0]

                RWE17I = ((RWE201) + (RWE202)) / 2

                WSU171 = data['WSU171']['list'][0]
                WSU172 = data['WSU172']['list'][0]

                WSU17I = ((WSU171) + (WSU172)) / 2

                EST17C = (AIR17I + LRS17I + BRS17I + NTS20I + WAC17I) / 5

                EPR17C = ((EMP20I) + (WTP17I) + (LNP17I)) / 3

                EMN17C = (CCM17I + RWE17I + WSU17I) / 3

                Ie = (EST17C + EPR17C + EMN17C) / 3

                AIDS_norm = data['AIDS']['list'][0]  # <-array(unlist(subset(ua, index_abbr == "AIDS", select = val)) )
                print('region ->>> ', region)
                try:

                    TSR_norm = data['TSR']['list'][0]
                except:
                    TSR_norm = 0.5

                HLT = (AIDS_norm + TSR_norm) / 2
                try:

                    EL_norm = data['EL']['list'][0]
                except:
                    EL_NORM = 0.5
                try:

                    LM_norm = data['LM']['list'][0]
                except:
                    LM_norm = 0.5
                # здоровля
                Ihd = 3 / 8 * (HLT) + 3 / 8 * (EL_norm) + 0.25 * (LM_norm)

                try:
                    PATLP = data['PATLP']['list'][0]
                except:
                    PATLP = 0.5

                try:
                    PATLSC = data['PATLSC']['list'][0]
                except:
                    PATLSC = 0.5

                try:
                    PATLG = data['PATLG']['list'][0]
                except:
                    PATLG = 0.5

                try:
                    PATLLA = data['PATLLA']['list'][0]
                except:
                    PATLLA = 0.5

                try:
                    WPSPA = data['WPSPA']['list'][0]
                except:
                    WPSPA = 0.5
                try:
                    CFL = data['CFL']['list'][0]
                except:
                    CFL = 0.5
                try:
                    PR = data['PR']['list'][0]
                except:
                    PR = 0.5

                PCP = (PATLP +
                       PATLSC +
                       PATLG +
                       PATLLA +
                       WPSPA +
                       CFL +
                       PR) / 7
                MPO_norm = data['MPO']['list'][0]
                SA_norm = data['SA']['list'][0]
                print(year, '!!!!!!!!!!!!')
                try:

                    CaPOTL_norm = data['CaPOTL']['list'][0]
                except:
                    CaPOTL_norm = 0.5

                DCS = (MPO_norm + SA_norm + CaPOTL_norm) / 3
                MmTL = data['MmTL']['list'][0]
                IU = data['IU']['list'][0]
                CS = data['CS']['list'][0]
                MmINT = data['MmINT']['list'][0]
                DIS = (MmTL + IU + MmINT) / 4
                NRO_norm = data['NRO']['list'][0]
                CTL_norm = data['CTL']['list'][0]
                REL = ((NRO_norm) + (CTL_norm)) / 2
                IID = 0.35 * (PCP) + 0.25 * (DCS) + 0.3 * (DIS) + 0.1 * (REL)
                try:

                    PPBD = data['PPBD']['list'][0]
                except:
                    PPBD = 0.5
                try:
                    SHF = data['SHF']['list'][0]
                except:
                    SHF = 0.5
                try:

                    AHP = data['AHP']['list'][0]
                except:
                    AHP = .5
                try:

                    NCPU = data['NCPU']['list'][0]
                except:
                    NCPU = 0.5
                try:

                    PSPTO = data['PSPTO']['list'][0]
                except:
                    PSPTO = 0.5
                try:

                    CPED = data['CPED']['list'][0]
                except:
                    CPED = 0.5
                INF = ((PPBD) +
                       (SHF) +
                       (AHP) +
                       (NCPU) +
                       (PSPTO) +
                       (CPED)) / 6
                DOGCO = data['DOGCO']['list'][0]
                COV = data['COV']['list'][0]
                NMBE = data['NMBE']['list'][0]
                RS = ((DOGCO) + (COV) + (NMBE)) / 3
                try:
                    LS = data['LS']['list'][0]
                except:
                    LS = 0.5
                try:
                    Illw = data['Illw']['list'][0]
                except:
                    Illw = 0.5
                try:
                    FNADAM = data['FNADAM']['list'][0]
                except:
                    FNADAM = 0.5

                try:
                    GRP = data['GRP']['list'][0]
                except:
                    GRP = 0.5
                EW = ((LS) + (Illw) + (FNADAM) + (GRP)) / 4

                # Економічне благополуччя
                Iql = 0.35 * INF + 0.35 * RS + 0.3 * EW

                Is = (Ihd + IID + Iql) / 3
                Cql = math.sqrt(Ie ** 2 + Is ** 2 + Iec ** 2)

                try:
                    BN = data['BN']['list'][0]
                except:
                    BN = 0.5

                try:
                    KPVM = data['KPVM']['list'][0]
                except:
                    KPVM = 0.5

                try:
                    DNO = data['DNO']['list'][0]
                except:
                    DNO = 0.5

                Sec = ((BN) ** 3 + (KPVM) ** 3 + (DNO) ** 3) ** (1 / 3)

                try:
                    WRS19 = data['WRS19']['list'][0]
                except:
                    WRS19 = 0.5

                try:
                    POL19 = data['POL19']['list'][0]
                except:
                    POL19 = 0.5
                try:
                    CCH19 = data['CCH19']['list'][0]
                except:
                    CCH19 = 0.5
                Se = ((WRS19) + (POL19) + (CCH19)) / 3

                try:
                    mortTB = 1 - data['mortTB']['list'][0]
                except:
                    mortTB = 0.5

                try:
                    mortCANC = 1 - data['mortCANC']['list'][0]
                except:
                    mortCANC = 0.5

                try:
                    mortCIRCUL = 1 - data['mortCIRCUL']['list'][0]
                except:
                    mortCIRCUL = 0.5

                try:
                    mortAIDS = 1 - data['mortAIDS']['list'][0]
                except:
                    mortAIDS = 0.5

                HEALTH_DEC = 0.2359512471 * (mortTB) + 0.236311309 * (mortCANC) + 0.2899148047 * (
                    mortCIRCUL) + 0.2378226392 * (mortAIDS)

                try:
                    CORR_PER = data['CFL']['list'][0]
                except:
                    CORR_PER = 0.5

                try:
                    LIFE_EXP = data['LIFE_EXP']['list'][0]
                except:
                    LIFE_EXP = 0.5

                try:
                    CRIME = data['CRIME']['list'][0]
                except:
                    CRIME = 0.5

                try:
                    CIR = data['CIR']['list'][0]
                except:
                    CIR = 0.5

                Ss = ((LIFE_EXP) ** 3 +
                      (CRIME) ** 3 +
                      (CORR_PER) ** 3 +
                      (CIR) ** 3 +
                      (HEALTH_DEC) ** 3) ** (1 / 3)

                TH_N_CURR_Y = 'TH_N_{}'.format(year)

                TH_N_CURR = data[TH_N_CURR_Y]['list'][0]

                TH_N_PREV1_Y = 'TH_N_{}'.format(year - 1)
                TH_N_PREV1 = data[TH_N_PREV1_Y]['list'][0]
                TH_N_PREV2_Y = 'TN_H_{}'.format(year - 2)
                try:

                    TH_N_PREV2 = data[TH_N_PREV2_Y]['list'][0]
                except:
                    TH_N_PREV2 = 0.5
                TH = ((TH_N_PREV2) + (TH_N_PREV1) + (
                    TH_N_CURR)) / 3  # TH = (TH_N_PREV2_Y + TH_N_PREV1_Y + TH_N_CURR_Y) / 3

                # техногенна загроза
                if TH:
                    So = 0.5
                else:
                    So = TH

                Csl = (Ss ** 3 + Sec ** 3 + Se ** 3 + So ** 3) ** (1 / 3)

                print('Csl', Csl)
                print('Ie', Ie)
                print('Is', Is)
                print('Iec', Iec)
                from cmath import sqrt
                Isd = sqrt((1 / 12 ** (1 / 3) * Csl) ** 2 + Ie ** 2 + Is ** 2 + Iec ** 2)

                G = 1 - math.acos(
                    (Iec + Ie + Is) / (math.sqrt(3.0) * math.sqrt(Iec ** 2 + Ie ** 2 + Is ** 2))) / math.acos(
                    1 / math.sqrt(3))
                result[region][str(year)] = {
                    'ISD': round(math.cos(cmath.polar(complex(Isd))[0]), 4),
                    'g': round(G, 4)
                }

                last_record = ExtraData.objects.all().last()
                if last_record:

                    ExtraData.objects.create(
                        LNP17I=LNP17I,
                        Ihd=Ihd,
                        WTP17I=WTP17I,
                        So=So,
                        Iql=Iql,
                        RWE17I=RWE17I,
                        Ss=Ss,
                        region=region,
                        g=G,
                        isd=Isd,
                        year=year,
                        order=last_record.order + 1
                    )
                else:
                    ExtraData.objects.create(
                        LNP17I=LNP17I,
                        Ihd=Ihd,
                        WTP17I=WTP17I,
                        So=So,
                        Iql=Iql,
                        RWE17I=RWE17I,
                        Ss=Ss,
                        region=region,
                        g=G,
                        isd=Isd,
                        year=year,
                        order=1
                    )

                Q = sqrt(1 ** 2 * Csl ** 2 + (Ie ** 2 + Iec ** 2 + Is ** 2))

    return render(request, 'export_data.html', {
        'result': result
    })


import json

import csv
from django.http import HttpResponse
from datetime import datetime


def save_data(request):
    if request.method == 'POST':
        data = request.POST.get('data')
        pre_clean_data = data.replace("'", '"')
        clean_data = json.loads(pre_clean_data)
        response = HttpResponse(content_type='text/csv')

        response['Content-Disposition'] = 'attachment; filename="export_ISD_G_{}.csv"'.format(
            str(datetime.now()).replace(' ', '_'))
        writer = csv.writer(response)
        writer.writerow(['Регіон', "Рік", "Індекс сталого розвитку", "Ступінь гармонізації "])
        for region, data in clean_data[0].items():
            for year, value in data.items():
                Data.objects.create(year=year, select_region=region, isd=value['ISD'], g=value['g'])
                writer.writerow([region, year, value['ISD'], value['g']])
                # writer.writerow([region, year, value['ISD'], value['g']])
        print('clean_data ', clean_data)

        return response
