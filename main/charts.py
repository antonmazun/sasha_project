from django.shortcuts import render, redirect
from pyexcel_ods import get_data
from .models import File, Data, ExtraData
from django.http import JsonResponse
import cmath, math
from statistics import mean

from .views import REGIONS


def chart_garmonizacii(request):
    list_names = ['водне навантаження', 'Індекс здоровя',
                  'забруднення повітря', 'техногенна загроза',
                  'Економічне благополуччя', 'змінювання клімату',
                  'корупція', 'Ступінь гармонізації', 'Індекс сталого розвитку']
    map_data = {key: [] for key in list_names}
    print(map_data)
    result = {}
    result_g = {}
    all_datas = ExtraData.objects.all()
    for data in all_datas:
        map_data['водне навантаження'].append(float(data.LNP17I))
        map_data['Індекс здоровя'].append(float(data.Ihd))
        map_data['забруднення повітря'].append(float(data.WTP17I))
        map_data['техногенна загроза'].append(float(data.So))
        map_data['Економічне благополуччя'].append(float(data.Iql))
        map_data['змінювання клімату'].append(float(data.RWE17I))
        map_data['корупція'].append(math.cos(cmath.polar(complex(data.Ss))[0]))
        map_data['Ступінь гармонізації'].append(float(data.g))
        map_data['Індекс сталого розвитку'].append(data.isd)

    # середнє значення індексу сталого розвитку
    mean_isd = mean(list(map(lambda x: math.cos(cmath.polar(complex(x))[0]), map_data['Індекс сталого розвитку'])))

    # середнє значення Ступінь гармонізації
    mean_g = mean(map_data['Ступінь гармонізації'])

    mean_LNP17I = mean(map_data['водне навантаження'])  # середнє для води
    mean_Ihd = mean(map_data['Індекс здоровя'])  # середнє для Індекс здоровя
    mean_WTP17I = mean(map_data['забруднення повітря'])  # середнє для забруднення повітря
    mean_So = mean(map_data['техногенна загроза'])  # середнє для техногенна загроза
    mean_Iql = mean(map_data['Економічне благополуччя'])  # середнє для Економічне благополуччя
    mean_RWE17I = mean(map_data['змінювання клімату'])  # середнє для змінювання клімату
    mean_Ss = mean(map_data['корупція'])  # середнє для корупція

    print("середнє значення індексу сталого розвитку", mean_isd)
    print("середнє водне навантаження", mean_LNP17I)

    nominator_result = 0  # чисельник
    denominator_result = 0  # знаменник
    left_den_result = 0  # ліва частина знаменника
    right_den_result = 0  # права частина знаменника
    isd = map_data['Індекс сталого розвитку']
    for index, xi in enumerate(map_data['водне навантаження']):
        isd_number = math.cos(cmath.polar(complex(isd[index]))[0])
        nominator_result += (xi - mean_LNP17I) * (isd_number - mean_isd)
        left_den_result += (xi - mean_LNP17I) ** 2
        right_den_result += (isd_number - mean_isd) ** 2
    denominator_result = math.sqrt(left_den_result * right_den_result)

    r_LNP17I = nominator_result / denominator_result
    result['r_LNP17I'] = round(r_LNP17I, 4)

    """ індекс здоровля """
    nominator_result = 0  # чисельник
    denominator_result = 0  # знаменник
    left_den_result = 0  # ліва частина знаменника
    right_den_result = 0  # права частина знаменника
    for index, xi in enumerate(map_data['Індекс здоровя']):
        isd_number = math.cos(cmath.polar(complex(isd[index]))[0])
        nominator_result += (xi - mean_Ihd) * (isd_number - mean_isd)
        left_den_result += (xi - mean_Ihd) ** 2
        right_den_result += (isd_number - mean_isd) ** 2
    denominator_result = math.sqrt(left_den_result * right_den_result)

    r_Ihd = nominator_result / denominator_result
    result['r_Ihd'] = round(r_Ihd, 4)

    """ забруднення повітря """
    nominator_result = 0  # чисельник
    denominator_result = 0  # знаменник
    left_den_result = 0  # ліва частина знаменника
    right_den_result = 0  # права частина знаменника
    for index, xi in enumerate(map_data['забруднення повітря']):
        isd_number = math.cos(cmath.polar(complex(isd[index]))[0])
        nominator_result += (xi - mean_WTP17I) * (isd_number - mean_isd)
        left_den_result += (xi - mean_WTP17I) ** 2
        right_den_result += (isd_number - mean_isd) ** 2
    denominator_result = math.sqrt(left_den_result * right_den_result)

    r_WTP17I = nominator_result / denominator_result
    result['r_WTP17I'] = round(r_WTP17I, 4)

    """ техногенна загроза """
    nominator_result = 0  # чисельник
    denominator_result = 0  # знаменник
    left_den_result = 0  # ліва частина знаменника
    right_den_result = 0  # права частина знаменника
    for index, xi in enumerate(map_data['техногенна загроза']):
        isd_number = math.cos(cmath.polar(complex(isd[index]))[0])
        nominator_result += (xi - mean_LNP17I) * (isd_number - mean_isd)
        left_den_result += (xi - mean_LNP17I) ** 2
        right_den_result += (isd_number - mean_isd) ** 2
    denominator_result = math.sqrt(left_den_result * right_den_result)

    r_So = nominator_result / denominator_result
    result['r_So'] = round(r_So * 1e16, 4)

    """ Економічне благополуччя """
    nominator_result = 0  # чисельник
    denominator_result = 0  # знаменник
    left_den_result = 0  # ліва частина знаменника
    right_den_result = 0  # права частина знаменника
    for index, xi in enumerate(map_data['Економічне благополуччя']):
        isd_number = math.cos(cmath.polar(complex(isd[index]))[0])
        nominator_result += (xi - mean_Iql) * (isd_number - mean_isd)
        left_den_result += (xi - mean_Iql) ** 2
        right_den_result += (isd_number - mean_isd) ** 2
    denominator_result = math.sqrt(left_den_result * right_den_result)

    r_Iql = nominator_result / denominator_result
    result['r_Iql'] = round(r_Iql, 4)

    """ змінювання клімату """
    nominator_result = 0  # чисельник
    denominator_result = 0  # знаменник
    left_den_result = 0  # ліва частина знаменника
    right_den_result = 0  # права частина знаменника
    for index, xi in enumerate(map_data['змінювання клімату']):
        isd_number = math.cos(cmath.polar(complex(isd[index]))[0])
        nominator_result += (xi - mean_RWE17I) * (isd_number - mean_isd)
        left_den_result += (xi - mean_RWE17I) ** 2
        right_den_result += (isd_number - mean_isd) ** 2
    denominator_result = math.sqrt(left_den_result * right_den_result)

    r_RWE17I = nominator_result / denominator_result
    result['r_RWE17I'] = round(r_RWE17I, 4)

    """ корупція """
    nominator_result = 0  # чисельник
    denominator_result = 0  # знаменник
    left_den_result = 0  # ліва частина знаменника
    right_den_result = 0  # права частина знаменника
    for index, xi in enumerate(map_data['корупція']):
        isd_number = math.cos(cmath.polar(complex(isd[index]))[0])
        nominator_result += (xi - mean_Ss) * (isd_number - mean_isd)
        left_den_result += (xi - mean_Ss) ** 2
        right_den_result += (isd_number - mean_isd) ** 2
    denominator_result = math.sqrt(left_den_result * right_den_result)

    r_Ss = nominator_result / denominator_result
    result['r_Ss'] = round(r_Ss, 4)

    """
        Кореляція для ступені гармонізації
    """

    """ корупція """
    nominator_result = 0  # чисельник
    denominator_result = 0  # знаменник
    left_den_result = 0  # ліва частина знаменника
    right_den_result = 0  # права частина знаменника
    for index, xi in enumerate(map_data['корупція']):
        isd_number = math.cos(cmath.polar(complex(isd[index]))[0])
        nominator_result += (xi - mean_Ss) * (isd_number - mean_g)
        left_den_result += (xi - mean_Ss) ** 2
        right_den_result += (isd_number - mean_g) ** 2
    denominator_result = math.sqrt(left_den_result * right_den_result)

    r_Ss_g = nominator_result / denominator_result
    result_g['r_Ss'] = round(r_Ss_g, 4)

    """ змінювання клімату """
    nominator_result = 0  # чисельник
    denominator_result = 0  # знаменник
    left_den_result = 0  # ліва частина знаменника
    right_den_result = 0  # права частина знаменника
    for index, xi in enumerate(map_data['змінювання клімату']):
        isd_number = math.cos(cmath.polar(complex(isd[index]))[0])
        nominator_result += (xi - mean_RWE17I) * (isd_number - mean_g)
        left_den_result += (xi - mean_RWE17I) ** 2
        right_den_result += (isd_number - mean_g) ** 2
    denominator_result = math.sqrt(left_den_result * right_den_result)

    r_RWE17I_g = nominator_result / denominator_result
    result_g['r_RWE17I'] = round(r_RWE17I_g, 4)

    """ Економічне благополуччя """
    nominator_result = 0  # чисельник
    denominator_result = 0  # знаменник
    left_den_result = 0  # ліва частина знаменника
    right_den_result = 0  # права частина знаменника
    for index, xi in enumerate(map_data['Економічне благополуччя']):
        isd_number = math.cos(cmath.polar(complex(isd[index]))[0])
        nominator_result += (xi - mean_Iql) * (isd_number - mean_g)
        left_den_result += (xi - mean_Iql) ** 2
        right_den_result += (isd_number - mean_g) ** 2
    denominator_result = math.sqrt(left_den_result * right_den_result)

    r_Iql_g = nominator_result / denominator_result
    result_g['r_Iql'] = round(r_Iql_g, 4)

    """ техногенна загроза """
    nominator_result = 0  # чисельник
    denominator_result = 0  # знаменник
    left_den_result = 0  # ліва частина знаменника
    right_den_result = 0  # права частина знаменника
    for index, xi in enumerate(map_data['техногенна загроза']):
        isd_number = math.cos(cmath.polar(complex(isd[index]))[0])
        nominator_result += (xi - mean_LNP17I) * (isd_number - mean_g)
        left_den_result += (xi - mean_LNP17I) ** 2
        right_den_result += (isd_number - mean_g) ** 2
    denominator_result = math.sqrt(left_den_result * right_den_result)

    r_So_g = nominator_result / denominator_result
    result_g['r_So'] = round(r_So_g, 4)

    """ забруднення повітря """
    nominator_result = 0  # чисельник
    denominator_result = 0  # знаменник
    left_den_result = 0  # ліва частина знаменника
    right_den_result = 0  # права частина знаменника
    for index, xi in enumerate(map_data['забруднення повітря']):
        isd_number = math.cos(cmath.polar(complex(isd[index]))[0])
        nominator_result += (xi - mean_WTP17I) * (isd_number - mean_g)
        left_den_result += (xi - mean_WTP17I) ** 2
        right_den_result += (isd_number - mean_g) ** 2
    denominator_result = math.sqrt(left_den_result * right_den_result)

    r_WTP17I_g = nominator_result / denominator_result
    result_g['r_WTP17I'] = round(r_WTP17I_g, 4)

    """ індекс здоровля """
    nominator_result = 0  # чисельник
    denominator_result = 0  # знаменник
    left_den_result = 0  # ліва частина знаменника
    right_den_result = 0  # права частина знаменника
    for index, xi in enumerate(map_data['Індекс здоровя']):
        isd_number = math.cos(cmath.polar(complex(isd[index]))[0])
        nominator_result += (xi - mean_Ihd) * (isd_number - mean_g)
        left_den_result += (xi - mean_Ihd) ** 2
        right_den_result += (isd_number - mean_g) ** 2
    denominator_result = math.sqrt(left_den_result * right_den_result)

    r_Ihd_g = nominator_result / denominator_result
    result_g['r_Ihd'] = round(r_Ihd_g, 4)

    """ водне навантаження """
    nominator_result = 0  # чисельник
    denominator_result = 0  # знаменник
    left_den_result = 0  # ліва частина знаменника
    right_den_result = 0  # права частина знаменника
    isd = map_data['Індекс сталого розвитку']
    for index, xi in enumerate(map_data['водне навантаження']):
        isd_number = math.cos(cmath.polar(complex(isd[index]))[0])
        nominator_result += (xi - mean_LNP17I) * (isd_number - mean_g)
        left_den_result += (xi - mean_LNP17I) ** 2
        right_den_result += (isd_number - mean_g) ** 2
    denominator_result = math.sqrt(left_den_result * right_den_result)

    r_LNP17I_g = nominator_result / denominator_result
    result_g['r_LNP17I'] = round(r_LNP17I_g, 4)

    result_by_regions = {}

    # mean_g . mean_isd
    for reg in REGIONS:
        all_data_by_region = ExtraData.objects.filter(region=reg)
        if all_data_by_region:
            result_by_regions[reg] = {
                'isd': [math.cos(cmath.polar(complex(d.isd))[0]) for d in all_data_by_region],
                'g': [float(d.g) for d in all_data_by_region],
            }

    # print('result_by_regions ', result_by_regions)
    w = {}

    for region, datas in result_by_regions.items():
        new_nominator = 0
        new_left_den_result = 0
        new_right_den_result = 0
        w.update({
            region: {
                'isd': round(mean(datas.get('isd')), 4),
                'g': round(mean(datas.get('g')), 4),
            }
        })
        # nominator_result = 0
        # left_den_result = 0
        # right_den_result = 0
        # w.update({region: mean(datas.get('isd'))})
        # for index, xi in enumerate(datas.get('isd')):
        #     print('!!!!!!!!!!!!!!!!!!1', xi, mean_isd)
        #     new_nominator += (xi - mean_isd) * (xi - mean_isd)
        #     print('new_nominator ', new_nominator)
        #     # nominator_result += (xi - mean_isd) * (xi - mean_isd)
        #     new_left_den_result += (xi - mean_isd) ** 2
        #     new_right_den_result += (xi - mean_isd) ** 2
        #     # left_den_result += (xi - mean_isd) ** 2
        #     # right_den_result += (xi - mean_isd) ** 2
        # print('XXXX',  new_nominator / math.sqrt(new_left_den_result * new_right_den_result))
        # # denominator_result = math.sqrt(left_den_result * right_den_result)
        # w.update({
        #     region: {
        #         'isd': round(nominator_result / denominator_result, 4)
        #     }})
        # print('***' * 20)

    # for

    return render(request, 'charts/chart_garmonizacii.html', {
        'result_isd': result,
        'result_g': result_g,
        'w': w,
        'list': [_ for _ in range(25)],
        'regions': REGIONS,
        'colspan': len(REGIONS) + 1
    })


def get_data_by_chart_garmonizacii(request):
    year = request.GET.get('year', 2018)
    print(year, '!!!!!!!!!!!')
    datas = Data.objects.filter(year=int(year))
    regions = [{d.select_region: d.g} for d in datas]
    return JsonResponse({
        'regions': regions
    })


def get_data_by_chart_isd(request):
    year = request.GET.get('year', 2018)

    datas = Data.objects.filter(year=int(year))
    regions = []
    for d in datas:
        complex_number = complex(d.isd)
        polar = cmath.polar(complex_number)[0]
        regions.append({d.select_region: abs(math.cos(polar))})
    return JsonResponse({
        'regions': regions
    })


def charts_bublle(request):
    result_dict = {}
    for region in REGIONS:
        data_by_region = ExtraData.objects.filter(region=region)
        result_dict.update({
            region: {
                'LNP17I': [float(d.LNP17I) for d in data_by_region],
                'Ihd': [float(d.Ihd) for d in data_by_region],
                'WTP17I': [float(d.WTP17I) for d in data_by_region],
                'So': [float(d.So) for d in data_by_region],
                'Iql': [float(d.Iql) for d in data_by_region],
                'RWE17I': [float(d.RWE17I) for d in data_by_region],
                'Ss': [math.cos(cmath.polar(complex(d.Ss))[0]) for d in data_by_region],
                'isd': [math.cos(cmath.polar(complex(d.isd))[0]) for d in data_by_region],
            }
        })
    r = {}
    for region, values in result_dict.items():
        r.update({region: {}})
        isd_region = values['isd']
        # середні значення для кожного коеф. кожного регіону
        mean_LNP17I = mean(values['LNP17I'])
        mean_Ihd = mean(values['Ihd'])
        mean_WTP17I = mean(values['WTP17I'])
        mean_So = mean(values['So'])
        mean_Iql = mean(values['Iql'])
        mean_RWE17I = mean(values['RWE17I'])
        mean_Ss = mean(values['Ss'])
        print('REGION ------>>>> ', region, mean_Ss)
        # середнє значення індексу сталого розвитку для регіону
        mean_ISD = mean(isd_region)

        LNP17I_nominator = 0
        left_LNP17I = 0
        right_LNP17I = 0
        for val, xi in enumerate(values['LNP17I']):
            LNP17I_nominator += (xi - mean_LNP17I) * (isd_region[val] - mean_ISD)
            left_LNP17I += (xi - mean_LNP17I) ** 2
            right_LNP17I += (isd_region[val] - mean_ISD) ** 2
        LNP17I_denominator = math.sqrt(left_LNP17I * right_LNP17I)
        LNP17I_r = round(LNP17I_nominator / LNP17I_denominator, 4)

        WTP17I_nominator = 0
        left_WTP17I = 0
        right_WTP17I = 0
        for val, xi in enumerate(values['WTP17I']):
            WTP17I_nominator += (xi - mean_WTP17I) * (isd_region[val] - mean_ISD)
            left_WTP17I += (xi - mean_WTP17I) ** 2
            right_WTP17I += (isd_region[val] - mean_ISD) ** 2
        WTP17I_denominator = math.sqrt(left_WTP17I * right_WTP17I)
        WTP17I_r = round(WTP17I_nominator / WTP17I_denominator, 4)

        RWE17I_nominator = 0
        left_RWE17I = 0
        right_RWE17I = 0
        for val, xi in enumerate(values['RWE17I']):
            RWE17I_nominator += (xi - mean_WTP17I) * (isd_region[val] - mean_ISD)
            left_RWE17I += (xi - mean_RWE17I) ** 2
            right_RWE17I += (isd_region[val] - mean_ISD) ** 2
        RWE17I_denominator = math.sqrt(left_RWE17I * right_RWE17I)
        RWE17I_r = round(RWE17I_nominator / RWE17I_denominator, 4)

        Iql_nominator = 0
        left_Iql = 0
        right_Iql = 0
        for val, xi in enumerate(values['Iql']):
            Iql_nominator += (xi - mean_Iql) * (isd_region[val] - mean_ISD)
            left_Iql += (xi - mean_Iql) ** 2
            right_Iql += (isd_region[val] - mean_ISD) ** 2
        Iql_denominator = math.sqrt(left_Iql * right_Iql)
        Iql_r = round(Iql_nominator / Iql_denominator, 4)

        Ihd_nominator = 0
        left_Ihd = 0
        right_Ihd = 0
        for val, xi in enumerate(values['Ihd']):
            Ihd_nominator += (xi - mean_Ihd) * (isd_region[val] - mean_ISD)
            left_Ihd += (xi - mean_Ihd) ** 2
            right_Ihd += (isd_region[val] - mean_ISD) ** 2
        Ihd_denominator = math.sqrt(left_Ihd * right_Ihd)
        Ihd_r = round(Ihd_nominator / Ihd_denominator, 4)

        Ss_nominator = 0
        left_Ss = 0
        right_Ss = 0
        for val, xi in enumerate(values['Ss']):
            Ss_nominator += (xi - mean_Ss) * (isd_region[val] - mean_ISD)
            left_Ss += (xi - mean_Ss) ** 2
            right_Ss += (isd_region[val] - mean_ISD) ** 2
        Ss_denominator = math.sqrt(left_Ss * right_Ss)
        Ss_r = round(Ss_nominator / Ss_denominator, 4)

        So_nominator = 0
        left_So = 0
        right_So = 0
        for val, xi in enumerate(values['So']):
            So_nominator += (xi - mean_So) * (isd_region[val] - mean_ISD)
            left_So += (xi - mean_So) ** 2
            right_So += (isd_region[val] - mean_ISD) ** 2
        So_denominator = math.sqrt(left_So * right_So) or 1
        So_r = round(So_nominator or 1 / So_denominator, 4)

        r[region].update({
            'LNP17I_r': LNP17I_r,
            'WTP17I_r': WTP17I_r,
            'RWE17I_r': RWE17I_r,
            'Iql_r': Iql_r,
            'Ihd_r': Ihd_r,
            'Ss_r': Ss_r,
            'So_r': So_r,
        })

    # bublle charts
    result_by_regions = {}
    for reg in REGIONS:
        all_data_by_region = ExtraData.objects.filter(region=reg)
        if all_data_by_region:
            result_by_regions[reg] = {
                'isd': [math.cos(cmath.polar(complex(d.isd))[0]) for d in all_data_by_region],
                'g': [float(d.g) for d in all_data_by_region],
                'ss': [math.cos(cmath.polar(complex(d.Ss))[0]) for d in all_data_by_region]
            }

        # print('result_by_regions ', result_by_regions)
    w = {}
    STATIC_COEF = 0.2
    for region, datas in result_by_regions.items():
        new_nominator = 0
        new_left_den_result = 0
        new_right_den_result = 0
        isd = abs(round(mean(datas.get('isd')), 4))
        g = abs(round(mean(datas.get('g')), 4))
        ss = abs(round(mean(datas.get('ss')), 4))
        if isd < STATIC_COEF and g < STATIC_COEF:
            continue
        else:
            w.update({
                region: {
                    'y': isd,
                    'x': g,
                    'z': ss,
                }
            })
    result_list = []
    for region, data in w.items():
        obj = {
            'country': region,
            'name': region[:3],
        }
        obj.update(**data)
        result_list.append(obj)
    return render(request, 'charts/charts_bublle.html', {
        'r': r,
        'result': result_dict,
        'w': w,
        'result_list': result_list,
    })


def get_data_by_coef(request):
    coef = request.GET.get('coef')

    result_by_regions = {}
    for reg in REGIONS:
        all_data_by_region = ExtraData.objects.filter(region=reg)
        if all_data_by_region:
            result_by_regions[reg] = {
                'isd': [math.cos(cmath.polar(complex(d.isd))[0]) for d in all_data_by_region],
                'g': [float(d.g) for d in all_data_by_region],
                # 'ss': [math.cos(cmath.polar(complex(d.Ss))[0]) for d in all_data_by_region]
            }
            if coef == 'Ss':
                result_by_regions[reg].update({
                    coef: [math.cos(cmath.polar(complex(d.Ss))[0]) for d in all_data_by_region]
                })
            elif coef == 'Ihd':
                result_by_regions[reg].update({
                    coef: [float(d.Ihd) for d in all_data_by_region]
                })
            elif coef == 'Iql':
                result_by_regions[reg].update({
                    coef: [float(d.Iql) for d in all_data_by_region]
                })
            elif coef == 'RWE17I':
                result_by_regions[reg].update({
                    coef: [float(d.RWE17I) for d in all_data_by_region]
                })
            elif coef == 'WTP17I':
                result_by_regions[reg].update({
                    coef: [float(d.WTP17I) for d in all_data_by_region]
                })
            elif coef == 'So':
                result_by_regions[reg].update({
                    coef: [float(d.So) for d in all_data_by_region]
                })

        # print('result_by_regions ', result_by_regions)
    w = {}
    STATIC_COEF = 0.2
    for region, datas in result_by_regions.items():
        new_nominator = 0
        new_left_den_result = 0
        new_right_den_result = 0
        isd = abs(round(mean(datas.get('isd')), 4))
        g = abs(round(mean(datas.get('g')), 4))
        coef_val = abs(round(mean(datas.get(coef)), 4))
        if isd < STATIC_COEF and g < STATIC_COEF:
            continue
        else:
            w.update({
                region: {
                    'y': isd,
                    'x': g,
                    'z': coef_val,
                }
            })
    result_list = []
    for region, data in w.items():
        obj = {
            'country': region,
            'name': region[:3],
        }
        obj.update(**data)
        result_list.append(obj)
    return JsonResponse({
        'result': result_list
    })
