from django.urls import path
from . import views
from . import charts

urlpatterns = [
    path('', views.main),
    path('history-download/', views.history_download),
    path('data-by-region/', views.data_by_region),
    path('export-data/', views.export_data),
    path('save-data/', views.save_data),
]

charts_urls = [
    path('chart-garmonizacii/', charts.chart_garmonizacii),
    path('get_data_by_chart_garmonizacii/', charts.get_data_by_chart_garmonizacii),
    path('get_data_by_chart_isd/', charts.get_data_by_chart_isd),
    path('charts-bublle/', charts.charts_bublle),
    path('get-data-by-coef/', charts.get_data_by_coef),
]
urlpatterns += charts_urls
