from django.db import models


# Create your models here.


class Test(models.Model):
    a = models.BooleanField()
    b = models.BooleanField()


class File(models.Model):
    file = models.FileField(upload_to='files/')
    select_region = models.CharField(max_length=255)
    create_at = models.DateTimeField(auto_created=True, auto_now_add=True)
    __str__ = lambda self: self.select_region


class Data(models.Model):
    select_region = models.CharField(max_length=255)
    year = models.IntegerField()
    isd = models.CharField(max_length=500, verbose_name='Індекс сталого розвитку (ISD)')
    g = models.CharField(max_length=500, verbose_name='Ступінь гармонізації (G)')
    create_at = models.DateTimeField(auto_created=True, auto_now_add=True)

    __str__ = lambda self: '{} {}'.format(self.select_region, self.year)


class ExportFile(models.Model):
    file = models.FileField(upload_to='export/')


class ExtraData(models.Model):
    LNP17I = models.CharField(max_length=255, verbose_name='водне навантаження', blank=True, null=True)
    Ihd = models.CharField(max_length=255, verbose_name='Індекс здоровя', blank=True, null=True)
    WTP17I = models.CharField(max_length=255, verbose_name='забруднення повітря', blank=True, null=True)
    So = models.CharField(max_length=255, verbose_name='техногенна загроза', blank=True, null=True)
    Iql = models.CharField(max_length=255, verbose_name='Економічне благополуччя', blank=True, null=True)
    RWE17I = models.CharField(max_length=255, verbose_name='змінювання клімату', blank=True, null=True)
    Ss = models.CharField(max_length=255, verbose_name='корупція', blank=True, null=True)

    region = models.CharField(max_length=255, verbose_name='REgion', blank=True, null=True)
    g = models.CharField(max_length=255, verbose_name='Ступінь гармонізації', blank=True, null=True)
    isd = models.CharField(max_length=255, verbose_name='Індекс сталого розвитку', blank=True, null=True)
    year = models.IntegerField(blank=True, null=True)
    order = models.IntegerField(default=1)

    __str__ = lambda self: '{} {}'.format(self.region, self.year)
