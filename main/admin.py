from django.contrib import admin
from .models import Data, File , ExtraData

# Register your models here.

admin.site.register(File)
admin.site.register(Data)
admin.site.register(ExtraData)

# class TestAdmin(admin.ModelAdmin):
#     list_display = ['_get_id', 'camera_name']
#
#     def _get_id(self, obj):
#         return obj.id
#
#     _get_id.short_description = 'ID'
#
#
# admin.site.register(Test, TestAdmin)
